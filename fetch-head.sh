#!usr//bin/env bash
# Set the 'URL' in your shell or just set it here.
#URL=""

# Downloads the commit patch and read the commit hash from it.
# Commit patches are in the form of:
# From  <commit SHA> <date> <time>
# We just read the second word from the first line from the patch which is the commit SHA.

if [[ -z ${URL} ]]; then
	echo "URL must not be empty, use export URL=\"\". You may also hard code the URL by setting it in the script.\n"
	exit 0
fi

if [ -f "new.txt" ]; then
	mv new.txt old.txt
	#OLD=$(head -n1 old.txt | awk '{print $2}')
fi
	OLD=$(head -n1 old.txt | awk '{print $2}')
	#echo $OLD
	curl "${URL}/commit/HEAD.patch" -o new.txt
	#head -n1 new.txt | awk '{print $2}'
	NEW=$(head -n1 new.txt | awk '{print $2}')
	#echo $NEW

	# Cleanup, we don't want to create multiple files and end up with an older commit in the process whilst creating duplicate files.
	#rm new.txt

echo $OLD
echo $NEW

BUILD=0

if [ ${NEW} != ${OLD} ]; then
	echo "Different commits detected, triggering build."
	BUILD=1
else
	echo "No new push detected, exiting..."
#	exit 0
fi

BUILD=1

if (( ${BUILD} == 1 )); then
	echo "Building"
	if test -f "test.json"
	then
		echo "Test JSON found, continuing...\n"
	else
		echo "Error: No JSON file found, exiting..."; exit 1;
	fi
fi

jq -e -r '.docker' test.json
status=$?

if ! [ $status == 0 ]; then
	echo "Error: \$docker unset/wrong, exiting..."; exit 1;
fi

DOCKER=$(jq -e -r '.docker' test.json)
echo "Docker is: $DOCKER"

# This is where the fun starts.

# Run the docker image.
